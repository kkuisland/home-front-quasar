import createPersistedState from 'vuex-persistedstate'
// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default async ({ store }) => {
	window.setTimeout(() => {
		createPersistedState({
			paths: ['sampleStore'],
		})(store)
	}, 0)
}
