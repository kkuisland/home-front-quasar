const routes = [
	{
		path: '/',
		component: () => import('layouts/MainLayout.vue'),
		children: [{ path: '', component: () => import('pages/Index.vue') }],
	},
	{
		path: '/dev',
		component: () => import('layouts/MainLayout.vue'),
		children: [
			{ path: '', component: () => import('pages/dev/Main.vue') },
			{ path: 'set/quasar', component: () => import('pages/dev/SetQuasar.vue') },
			{ path: 'set/fastify', component: () => import('pages/dev/SetQuasar.vue') },
			{ path: 'jwt', component: () => import('pages/dev/Jwt.vue') },
		],
	},
	{
		path: '/music',
		component: () => import('layouts/MainLayout.vue'),
		children: [{ path: 'pop', component: () => import('pages/music/Pop.vue') }],
	},

	// Always leave this as last one,
	// but you can also remove it
	{
		path: '/:catchAll(.*)*',
		component: () => import('pages/Error404.vue'),
	},
]

export default routes
