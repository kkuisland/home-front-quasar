'use strict'

import { api } from 'src/boot/axios'

// 생성
export const store = async (body) => {
	try {
		const response = await api.post('/api/menu', body)
		return response.data
	} catch (error) {
		console.error('%c> menu Store Error => ', 'background: #222; color: #f91a47; font-size: 24px;', error)
	}
}

// index 조회
export const get = async (_id) => {
	try {
		const response = await api.get(`/api/menu/${_id}`)
		return response.data
	} catch (error) {
		console.error('%c> menu Get Error => ', 'background: #222; color: #f91a47; font-size: 24px;', error)
	}
}

// 리스트 조회
export const fetch = async () => {
	try {
		const response = await api.get('/api/menu')
		return response.data
	} catch (error) {
		console.error('%c> menu Fetch  Error => ', 'background: #222; color: #f91a47; font-size: 24px;', error)
	}
}

// 업데이트
export const Update = async (body) => {
	try {
		const response = await api.put('/api/menu/', body)
		return response.data
	} catch (error) {
		console.error('%c> menu Update  Error => ', 'background: #222; color: #f91a47; font-size: 24px;', error)
	}
}

// 삭제
export const destroy = async (_id) => {
	try {
		const response = await api.delete(`/api/menu/${_id}`)
		return response.data
	} catch (error) {
		console.error('%c> menu Destroy  Error => ', 'background: #222; color: #f91a47; font-size: 24px;', error)
	}
}
