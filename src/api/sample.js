'use strict'

import { api } from 'src/boot/axios'

export const jwt = async () => {
	const response = await api.get('/api/sample/jwt')
	return response.data
}

export const check = async () => {
	const response = await api.get('/api/sample')
	return response.data
}
