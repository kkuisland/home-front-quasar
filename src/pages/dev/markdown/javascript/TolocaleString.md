## toLocaleString()

```js
var num = 1234.56789
console.log(num.toLocaleString('ar-EG'))
console.log(num.toLocaleString('zh-Hans-CN-u-nu-hanidec'))
console.log(num.toLocaleString('ko-KR', { style: 'currency', currency: 'KRW' }))

> ١٬٢٣٤٫٥٦٨
> 一,二三四.五六八
> ₩1,235
```

### (String) locales

생략하면 웹브라우저의 기본 Locale 값을 사용하지만 직접 지정할 수도 있다.

### (Object) options

다음의 속성들을 전부 또는 일부 포함한 객체로 지정할 수 있다.

```
localeMatcher :
지역화 일치 알고리즘을 'lookup' 또는 'best fit'로 지정한다. 기본 값은 'best fit'이다.
```

```
style :
사용할 서식 스타일이며 기본 값은 'decimal'이다.
다음 아래의 값으로 지정
'deciam': 일반 숫자
'currency': 통화 형식
'percent': 백분율 형식
```

```
currency :
통화 형식에 사용할 통화이며 가능한 값은 미국 달러 'USD', 유로화 'EUR', 중국 위안화 'CNY' 등과 같은 ISO 4217 통화 코드이다.
기본 값은 없으며 style 속성에서 'currency'를 설정해야 한다.
```

```
currencyDisplay :
통화를 통화 형식으로 표시하는 방법이며 가능한 값은 €와 같이 현지 통화 기호를 사용하는 'symbol',
ISO 통화 코드를 사용하는 'code', dollar와 같이 현지 통화 이름을 사용하는 'name'이 있다.
기본 값은 'symbol'이다.
```

```
useGrouping :
천 단위 또는 lakh, crore 구분 기호를 사용할지 여부를 boolean 값으로 지정한다.
기본 값은 true이다.
```

```
minimumIntegerDigits :
사용할 최소 정수 자릿수이며 가능한 값은 1~21사이의 값이다. 기본 값은 1이다.
```

```
minimumFractionDigits :
사용할 최소 소수 자릿수이며 가능한 값은 0~20사이의 값이다.
일반 숫자와 퍼센트 형식의 기본 값은 0이다. 통화 형식의 기본 값은 ISO 4217 통화 코드 목록에서 참고한다.
```

```
maximumFractionDigits :
사용할 최대 소수 자릿수이며 가능한 값은 0~20사이의 값이다. 일반 숫자 형식의 기본 값은 minimumFractionDigits와 3보다 크다.
통화 형식의 기본 값은 minimumFractionDigits 보다 크고 ISO 4217 통화 코드 목록에서 제공하는 부 단위의 숫자의 수이다.
```

```
minimumSignificantDigits :
사용할 유효 숫자의 최소 수이며 가능한 값은 1~21이다. 기본 값은 1이다.
```

```
maximumSignificantDigits :
사용할 최대 유효 자릿수이며 가능한 값은 1~21이다. 기본 값은 21이다.
```

---

출처> http://www.devdic.com/javascript/refer/native/method:1344/toLocaleString()
