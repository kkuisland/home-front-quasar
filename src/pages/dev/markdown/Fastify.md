# KKu Island ( Node Fastify Server )

# Getting Started with [Fastify-CLI](https://www.npmjs.com/package/fastify-cli)

This project was bootstrapped with Fastify-CLI.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

To start the app in dev mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm start`

For production mode

### `npm run test`

Run the test cases.

## Learn More

To learn Fastify, check out the [Fastify documentation](https://www.fastify.io/docs/latest/).

## NodeJS-Fastify with TypeScript

#### Fastify-cli 를 이용하여 세팅

[Fastify-cli](https://github.com/fastify/fastify-cli 'Fastify-cli Link')

```bash
# fastify-cli 글로벌 설치
npm install fastify-cli --global

# fastify 프로젝트 생성 JS 버전
fastify generate [ProjectName]
```

> 프로젝트 이동

```bash
# 프로젝트 이동
cd [ProjectName]

# 기본모듈 설치
npm install

# 실행 테스트 진행 | Dev port
npm run dev
- Server listening at http://127.0.0.1:3000
```

---

## 프로젝트 스택

- [ES Lint+Pritter](#ES-Lint+Pritter-세팅)
- [Mongoose](#Mongoose-세팅)
- [JWT](#JWT-세팅)
- [Cookie](#Cookie-세팅)
- [cors](#Cors-세팅)

---

#### ES-Lint+Pritter-세팅

```bash
# EsLint 설치, Prettier 설치
npm i -D eslint prettier
# prettier 플러그인
npm i -D eslint-config-prettier eslint-plugin-prettier
```

npm을 통해 타입스크립트와 eslint, prettier와 관련 플러그인을 설치한다.

```bash
# 리액트 관련 규칙 X
npm i -D eslint-config-airbnb-base
# 리액트 관련 규칙 O
npm i -D eslint-config-airbnb
# 리액트는 추가 설치
npm i -D eslint-plugin-react eslint-plugin-react-hooks
npm i -D eslint-plugin-jsx-a11y eslint-plugin-import
```

```js
// .eslintrc.js 설정
module.exports = {
	env: {
		browser: true,
		node: true, // 에러 방지 위해 browswer, node 둘다 true
		es2020: true, //2020-12-03 기준 2021은 eslint가 안되는 오류가 있다
	},
	extends: ['eslint:recommended', 'airbnb-base', 'plugin:prettier/recommended'],
	parserOptions: {
		ecmaVersion: 11, //es2020
		sourceType: 'module',
	},
	rules: {},
}
```

```js
// .prettierrc.js 설정
module.exports = {
	trailingComma: 'es5',
	tabWidth: 2,
	printWidth: 120,
	semi: false,
	singleQuote: true,
	bracketSpacing: true,
	arrowParens: 'always',
	proseWrap: 'preserve',
	htmlWhitespaceSensitivity: 'strict',
	endOfLine: 'lf',
	useTabs: true,
	jsxBracketSameLine: true,
}
```

#### Mongoose 세팅

```bash
# Mongoose 설치
npm install -S mongoose

#/src/plugins/mongoose.js 생성
#.env 파일에 MongoDB 커넥션
# MONGODB_URL = mongodb://아아디:패스워드@아아피:포트/콜렉션
```

```js
// Mongoose
'use strict'

const fp = require('fastify-plugin')
const mongoose = require('mongoose')

/**
 * 몽고 DB
 *
 * @see https://mongoosejs.com/docs
 */

module.exports = fp(async function (fastify, opts) {
	mongoose
		.connect(`${process.env.MONGODB_URL}`, {
			// useFindAndModify: false,
			// useNewUrlParser: true,
			// useUnifiedTopology: true,
		})
		.then((response) => {
			console.log('몽고 DB 연결 성공')
		})
		.catch((error) => {
			console.error('Error plugins mongoose', error)
		})
})
```

```shell
# .env
# 몽고DB URL
MONGODB_URL = mongodb://아아디:패스워드@아이피:포트/콜렉션
```

##### JWT 세팅

```bash
# JWT 설치
npm install -S fastify-jwt

#/src/plugins/jwt.js
```

```ts
import fp from 'fastify-plugin'
import fastifyJwt, { FastifyJWTOptions } from 'fastify-jwt'
/**
 * This plugins adds some utilities to handle http errors
 *
 * @see https://github.com/fastify/fastify-sensible
 */
export default fp<FastifyJWTOptions>(async (fastify, opts) => {
	const secretKey = Buffer.from(`${process.env.JWT_SECRET_KEY}`).toString('base64')
	fastify.register(fastifyJwt, {
		secret: secretKey,
		decode: { complete: true },
		sign: {
			// algorithm: "HS256",
			// issuer: "kkuisland.com",
			expiresIn: '7d',
			// subject: "kkuToken",
		},
		verify: {
			//   algorithms: "HS256",
			//   issuer: "kkuisland.com",
			maxAge: '7d',
			//   subject: "kkuToken",
		},
		cookie: {
			cookieName: 'kkuToken',
			signed: true,
		},
	})
})
```

```shell
# .env
# JWT
JWT_SECRET_KEY = domain.jwt
```

##### Cookie 세팅

    # Cookie 설치
    npm install -S fastify-cookie

    #/src/plugins/cookie.ts

```ts
import fp from 'fastify-plugin'
import fastifyCookie, { FastifyCookieOptions } from 'fastify-cookie'
/**
 * 쿠키 읽기 및 설정에 대한 지원을 추가하는 Fastify 용 플러그인 .
 * 이 플러그인의 쿠키 구문 분석은 Fastify의 onRequest후크 를 통해 작동합니다 .
 * 따라서 onRequest이 플러그인의 동작에 의존 하는 다른 후크 보다 먼저 등록해야 합니다 .
 * fastify-cookie v2.x 는 Fastify@1과 Fastify@2를 모두 지원합니다. fastify-cookie v3는 Fastify@2만 지원합니다.
 *
 * @see https://github.com/fastify/fastify-cookie
 */

export default fp<FastifyCookieOptions>(async function (fastify, opts) {
	fastify.register(fastifyCookie, {
		secret: `${process.env.COOKIE_SIGN_KEY}`,
	})
})
```

```shell
# .env
# COOKIE
COOKIE_SIGN_KEY = domain.cookie
```

##### Cors 세팅

```bash
# Cors 설치
npm install -S fastify-cors

#/src/plugins/cors.ts
```

```ts
// cors.ts
import fp from 'fastify-plugin'
import fastifyCors, { FastifyCorsOptions } from 'fastify-cors'
/**
 * fastify-corsFastify 애플리케이션에서 CORS 를 사용할 수 있습니다 .
 * Fastify 버전을 지원합니다 3.x. Fastify 호환성 은 이 분기 및 관련 버전을 참조하십시오 ^2.x. Fastify 호환성 은 이 분기 및 관련 버전을 참조하십시오 ^1.x.
 *
 * @see https://github.com/fastify/fastify-cors
 */

export default fp<FastifyCorsOptions>(async function (fastify, opts) {
	fastify.register(fastifyCors, {
		origin: true,
		credentials: true,
		methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
	})
})
```

##### custom util 세팅

```bash
# Util 설치

#/src/plugins/util.ts
```

```ts
// customUtil.ts
import fp from 'fastify-plugin'
// import { FastifyPluginCallback, FastifyPluginAsync } from 'fastify'
// import { FastifyPluginCallback } from 'fastify'
export interface SupportPluginOptions {
	// Specify Support plugin options here
}

// define options
module.exports = fp<SupportPluginOptions>(async function (fastify, opts) {
	/* 문자열 합치기 */
	fastify.decorate('concatenatingStrings', function (a: string, b: string) {
		return a + b
	})

	/* 테스트 URL */
	fastify.get('/plugin', (request, reply) => {
		const concat = fastify.concatenatingStrings('꾸', '인수')
		reply.send({ concat: concat })
		// reply.send(request.is(['html', 'json']))
		// throw fastify.httpErrors.notFound()
		// reply.vary('Accept')
		// reply.send('ok')
	})
})

// When using .decorate you have to specify added properties for Typescript
declare module 'fastify' {
	export interface FastifyInstance {
		concatenatingStrings(arg1: string, arg2: string): string
	}
}
```

## Router

라우터 세팅 /router
폴더명/index.ts
sample/:\_id(router url)

```ts
import { FastifyPluginAsync } from 'fastify'
import * as sampleController from '../../controllers/sample'

const sample: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
	// 생성
	fastify.post('/', sampleController.store)
	// 전체 리스트
	fastify.get('/', sampleController.fetch)
	// 인덱스 검색
	fastify.get('/:_id', sampleController.get)
	// 수정
	fastify.put('/', sampleController.update)
	// 삭제
	fastify.delete('/:_id', sampleController.destroy)
}

export default sample
```

## Model

모델 세팅 /src
폴더/모델명.ts
models/sample.ts

```ts
import { Schema, model, Document } from 'mongoose'
import * as moment from 'moment'

interface Sample extends Document {
	name: string
	sequence: number
	title: string
	remarks?: string
	createdAt: string
	updatedAt: string
}

const schema = new Schema<Sample>({
	// 이름
	name: { type: String, required: true, index: true },
	// 순번
	sequence: { type: Number, required: true },
	// 제목
	title: { type: String, required: true },
	// 비고사항
	remarks: { type: String, default: null },
	// 생성일자
	createdAt: { type: String, default: moment().format('YYYY-MM-DD hh:mm:ss') },
	// 수정일자
	updatedAt: { type: String, default: moment().format('YYYY-MM-DD hh:mm:ss') },
})

export default model<Sample>('Sample', schema, 'Sample')
```

## Controller

컨트롤러 세팅 /src
폴더/컨트롤러명.ts
controllers/sample.ts

```ts
import * as _ from 'lodash'

import Sample from '../models/sample'

// Sample 생성
export const store = async (request: any, reply: any) => {
	const reqBody = request.body
	let sample = null
	try {
		sample = await Sample.create(reqBody)
	} catch (error) {
		console.log('PartsController-Create-Error ->', error)
	}
	if (sample) {
		reply.send({ bool: true, value: sample, message: 'Sample 생성 완료' })
	} else {
		reply.send({ bool: false, value: null, message: 'Sample 생성 실패' })
	}
}
// Sample 리스트
export const fetch = async (request: any, reply: any) => {
	let sample = null
	try {
		sample = await Sample.find()
	} catch (error) {
		console.log('SampleController-Fetch-Error ->', error)
	}
	if (sample) {
		const newSample = sample.map((item) => {
			let rObj = {}
			console.log('item ->', item)
			rObj = { item }
			return rObj
		})
		reply.send({
			bool: true,
			value: newSample,
			message: 'Sample 리스트 검색 완료',
		})
	} else {
		reply.send({
			bool: false,
			value: null,
			message: 'Sample 리스트 검색 실패',
		})
	}
}
// Sample 인덱스 검색
export const get = async (request: any, reply: any) => {
	const { _id } = request.params

	let sample = null
	try {
		sample = await Sample.findById(_id)
	} catch (error) {
		console.log('SampleController-Get-Error ->', error)
	}
	if (sample) {
		// 토큰 데이터
		let tokenData = {
			id: sample.name,
			phone: '010-9947-0728',
		}
		// 토큰 데이터 추가하여 표기
		const newSample = _.merge({}, sample, { token: tokenData })
		// 토큰 생성
		let token = await reply.jwtSign(tokenData)
		// 토큰을 이용한 쿠키 생성
		await reply
			.setCookie('kkuToken', token, {
				// domain: '*',
				path: '/',
				httpOnly: true,
				secure: true,
				sameSite: 'None',
				overwrite: true,
				maxAge: 24 * 60 * 60 * 5,
				signed: true,
			})
			.send({ bool: true, value: newSample, message: 'Sample2 검색 완료' })
	} else {
		reply.send({ bool: false, value: null, message: 'Sample 검색 실패' })
	}
}
// Sample 수정
export const update = async (request: any, reply: any) => {
	const reqBody = request.body
	let sample = null
	try {
		sample = await Sample.findByIdAndUpdate(reqBody._id, { ...reqBody })
	} catch (error) {
		console.log('PartsController-Update-Error ->', error)
	}
	if (sample) {
		reply.send({ bool: true, value: sample, message: 'Sample 수정 완료' })
	} else {
		reply.send({ bool: false, value: null, message: 'Sample 수정 실패' })
	}
}
// Sample 인덱스 검색
export const destroy = async (request: any, reply: any) => {
	const { _id } = request.params
	let sample = null
	try {
		sample = await Sample.deleteOne({ _id: _id })
	} catch (error) {
		console.log('SampleController-Destroy-Error ->', error)
	}
	if (sample) {
		reply.send({ bool: true, value: sample, message: 'Sample 제거 완료' })
	} else {
		reply.send({ bool: false, value: null, message: 'Sample 제거 실패' })
	}
}
```
