# KKu Island ( Vue3 Quasar Framework )

KKu Island

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn run lint
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

---

## KKu Island 세팅

### Set Quasar

```bash
# 환경설정 등록 시킨다
yarn global bin
# Quasar cli 설치
yarn global add @quasar/cli
# Quasar Project 설치
quasar create [project name]
# 선택 옵션
Sass, Typescript, Vuex, Axios, Vue-I18n, Eslint, Prettier, Yarn
```

### Set Visual Studio Code

> 확장: 설치

- Debugger For Chrome
- Batter Comments
- DotENV
- Doxygen Documentation Generator
- JavaScript and TypeScript Nightly
- Quasar Extension Pack
- Quasar Snippets
- IntelliSense for CSS class names in HTML
- Todo Tree
- Vue 3 Snippets

### Set Prettier

```js
// .prettierrc.js 설정
module.exports = {
	trailingComma: 'es6',
	tabWidth: 2,
	printWidth: 120,
	semi: false,
	singleQuote: true,
	bracketSpacing: true,
	arrowParens: 'always',
	proseWrap: 'preserve',
	htmlWhitespaceSensitivity: 'strict',
	endOfLine: 'lf',
	useTabs: true,
	jsxBracketSameLine: true,
}
```

### Set Quasar FrameWork

```js
// quasar.config.js

build: {
  vueRouterMode: 'history'
},
devServer: {
  server: {
    type: 'https'
  },
  port: 9000,
  open: false
}.

```

### Set Style

> src/css/app.sass

```
// app global css in Sass form
@import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+KR:wght@100;200;300;400;500;600;700&family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap')

body
  font-family: 'IBM Plex Sans KR', sans-serif

body
  max-width: 1200px !important
  margin: 0 auto !important
  background-color: $glass

.q-toolbar
  max-width: 1200px !important
  margin: 0 auto !important
/** 개인옵션 **/
.my-card
  box-shadow: none
  border-radius: 3px
  padding: 0
  padding-top: 10px
  background-color: $glass
  border-bottom: 1px solid $californian-coral
  // border-top: 2px solid $bondi-blue

/* SCROLL */
.q-body--fullscreen-mixin, .q-body--prevent-scroll
  position: relative !important
/* 스크롤바의 width */
::-webkit-scrollbar
  width: 5px
/* 스크롤바의 전체 배경색 */
::-webkit-scrollbar-track
  background-color: #f0f0f0
/* 스크롤바 색 */
::-webkit-scrollbar-thumb
  background: linear-gradient(to bottom, #1697bf, #62b7ac)
/* 위 아래 버튼 (버튼 없애기를 함) */
::-webkit-scrollbar-button
  display: none
/* 스크롤바 없애기 */
// body
//   -ms-overflow-style: none
::-webkit-scrollbar
  display: none
/*특정 부분 스크롤바 없애기*/
// .box
//   -ms-overflow-style: none
// .box::-webkit-scrollbar
//   display:none

/** 컬러 설정 **/
.text-glass
  color: $glass
.text-chill
  color: $chill
.text-californian-coral
  color: $californian-coral
.text-bondi-blue
  color: $bondi-blue
.text-marina
  color: $marina
.text-sand
  color: $sand
.text-tree
  color: $tree

.bg-glass
  background-color: $glass
.bg-chill
  background-color: $chill
.bg-californian-coral
  background-color: $californian-coral
.bg-bondi-blue
  background-color: $bondi-blue
.bg-marina
  background-color: $marina
.bg-sand
  background-color: $sand
.bg-tree
  background-color: $tree
```

> src/css/quasar.variables.sass

```
// Quasar Sass (& SCSS) Variables
// --------------------------------------------------
// To customize the look and feel of this app, you can override
// the Sass/SCSS variables found in Quasar's source Sass/SCSS files.

// Check documentation for full list of Quasar variables

// Your own variables (that are declared here) and Quasar's own
// ones will be available out of the box in your .vue/.scss/.sass files

// It's highly recommended to change the default colors
// to match your app's branding.
// Tip: Use the "Theme Builder" on Quasar's documentation website.

// $primary   : #1976D2
$primary   : #1b47a5
// $secondary : #26A69A
$secondary : #bceadf
// $accent    : #9C27B0
$accent    : #5f7ab7

// $dark      : #1D1D1D
$dark      : #3a3e31

// $positive  : #21BA45
$positive  : #02938b
// $negative  : #C10015
$negative  : #f91a47
// $info      : #31CCEC
$info      : #1db9ce
// $warning   : #F2C037
$warning   : #e99a28

$glass : #edfafd
$chill : #aed9da
$californian-coral : #3ddad7
$bondi-blue : #2a93d5
$marina : #135589
$sand: #e1cdaf
$tree: #00A651
```

### Set Store

```bash
qusar new store sampleStore
```

```js
// store/sampleStore/action.js
module.exports = {
	setOne: (context, sample) => {
		context.commit('oneMutation', sample)
	},
}

// store/sampleStore/getter.js
module.exports = {
	info: (state) => {
		return state
	},
}

// store/sampleStore/mutation.js
module.exports = {
	oneMutation: (state, sample) => {
		// Object.assign(state, sample)
		state.sample = sample
	},
}

// store/sampleStore/state.js
export default function (sample) {
	return {
		sample,
	}
}

// store/index.js
import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import sampleStore from './sampleStore'

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
	const Store = createStore({
		modules: {
			sampleStore,
		},
		// enable strict mode (adds overhead!)
		// for dev mode and --debug builds only
		strict: process.env.DEBUGGING,
	})

	return Store
})

```

### Set persist-client ( 새로고침 store 유지)

```bash
yarn add vuex-persistedstate

quasar new boot persist-client
```

```js
// quasar.conf.js
boot: [{ pathe: 'persist-client', server: false }, 'i18n', 'axios'], // 서버는 false 로 설정 해준다.

// boot/persist-client.js
import createPersistedState from 'vuex-persistedstate'
// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default async ({ store }) => {
	window.setTimeout(() => {
		createPersistedState({
			paths: ['userStore'],
		})(store)
	}, 0)
}

```

### Set Quasar Extension ( Markdown )

```bash
quasar ext add @quasar/qmarkdown@next
```

```json
// quasar.extensions.json

{
	"@quasar/qmarkdown": {
		"import_md": true,
		"import_vmd": true
	}
}
```

```vue
<script>
'use strict'

import setQuasar from './markdown/quasar.md'
import commonQuasar from './commonQuasar.md'
import { useRouter } from 'vue-router'
import { onBeforeMount, onUpdated, ref } from 'vue'
export default {
	name: 'SetQuasar',

	setup() {
		const $router = useRouter()
		const markdownPage = ref()

		const setMarkdown = async () => {
			const pathParts = $router.currentRoute.value.path.split('/')

			switch (pathParts[3]) {
				case 'common':
					markdownPage.value = await commonQuasar
					break

				case 'quasar':
					markdownPage.value = await setQuasar
					break

				default:
					markdownPage.value = await commonQuasar
					break
			}
		}

		// 새로고침 실행
		onBeforeMount(() => {
			setMarkdown()
		})

		// 페이지 전환 실행
		onUpdated(() => {
			setMarkdown()
		})

		return { markdownPage }
	},
}
</script>

<template>
	<q-page padding>
		<div class="row">
			<div class="col bg-glass">
				<div class="row q-col-gutter-md">
					<!-- 메인 -->
					<div class="col-12">
						<q-markdown :src="markdownPage" />
					</div>
				</div>
			</div>
		</div>
		<q-page-scroller position="bottom-right" :scroll-offset="150" :offset="[18, 18]">
			<q-btn dense label="위로" color="sand" text-color="tree" size="sm" />
		</q-page-scroller>
	</q-page>
</template>

<style lang="sass" scoped></style>
```
