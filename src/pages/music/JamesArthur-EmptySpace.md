## James Arthur - Empty Space

<strong>I don't see you</strong>
널 생각하지 않아

<strong>You're not in every window I look through</strong>
보이는 모든 창문에 더는 네가 떠오르지 않아

<strong>And I don't miss you</strong>
네가 그립지 않아

<strong>You're not in every single thing I do</strong>
내 일상에 너는 이제 없어

<strong>I don't think we're meant to be</strong>
우린 운명이 아니란 생각이 들어

<strong>And you are not the missing piece</strong>
넌 내 반쪽이 아니야

<strong>I won't hear it</strong>
듣지 않을 꺼야

<strong>Whenever anybody says your name</strong>
누군가 네 이름을 말할 때말야

<strong>And I won't feel it</strong>
느끼고 싶지 않아

<strong>Even when I'm burstin' into flames</strong>
불길에 휩싸인다 하더라도

<strong>I don't regret the day I left</strong>
내가 떠난 그날을 후회하지 않아

<strong>I don't believe that I was blessed</strong>
내가 축복받았다고 믿지 않아

<strong>I'm probably lyin' to myself again</strong>
아마도 또 나에게 거짓말을 하고 있나 봐

<strong>I'm alone in my head</strong>
난 홀로 생각에 잠겼어

<strong>Lookin' for love in this stranger's bed</strong>
낯선 사람 침대에서 사랑을 찾고 있고

<strong>But I don't think I'll find it</strong>
근데 못찾을 것 같아

<strong>'Cause only you could fill this empty space</strong>
왜냐면 너만이 이 텅 빈 마음을 채울 수 있을테니까

<strong>I wanna tell all my friends</strong>
내 친구들에게 말하고 싶어

<strong>But I don't think they would understand</strong>
친구들이 이해할 거란 생각은 안해

<strong>It's somethin' I've decided</strong>
그냥 내가 결정한 일이지

<strong>'Cause only you could fill this empty space</strong>
너만이 이 텅 빈 마음을 채울 수 있을테니까

<strong>I've been drinking</strong>
요즘 술을 마시고 있어

<strong>I've been doin' things I shouldn't do</strong>
하면 안 되는 짓들만 골라 하고 있어

<strong>Overthinkin'</strong>
너무 생각이 많고

<strong>I don't know who I am without you</strong>
너 없는 난 누군지 모르겠어

<strong>I'm a liar and a cheat</strong>
난 거짓말쟁이에 사기꾼이야

<strong>I let my ego swallow me</strong>
내 자아가 날 삼키게 놔두고 있어

<strong>And that's why I might never see you again</strong>
그게 널 다신 볼 수 없는 이유인 것 같아

<strong>I'm alone in my head</strong>
난 홀로 생각에 잠겼어

<strong>Lookin' for love in this stranger's bed</strong>
낯선 사람 침대에서 사랑을 찾고 있어

<strong>But I don't think I'll find it</strong>
근데 못찾을 것 같다

<strong>'Cause only you could fill this empty space</strong>
왜냐면 너만이 이 텅 빈 마음을 채울 수 있을테니까

<strong>I wanna tell all my friends</strong>
내 친구들에게 말하고 싶어

<strong>But I don't think they would understand</strong>
친구들이 이해할 거란 생각은 하지 않아

<strong>It's somethin' I've decided</strong>
그냥 내가 결정한 일이지

<strong>'Cause only you could fill this empty space</strong>
너만이 이 텅 빈 마음을 채울 수 있을테니까

<strong>Space, space</strong>
<strong>this empty sapce</strong>
이 텅 빈 마음
<strong>Space, space</strong>
<strong>this ..</strong>
<strong>'Cause only you... thi empy sapce</strong>
너만이 이 텅 빈 마음을 채울 수 있을테니까

<strong>I couldn't make you love me</strong>
<strong>I couldn't make you love me</strong>
<strong>I couldn't make you love me</strong>
<strong>I couldn't make you love me</strong>

<strong>I couldn't make you love</strong>
<strong>I couldn't make you love me</strong>
<strong>I couldn't make you love me</strong>
