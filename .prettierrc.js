module.exports = {
	trailingComma: 'es5',
	tabWidth: 2,
	printWidth: 120,
	semi: false,
	singleQuote: true,
	bracketSpacing: true,
	arrowParens: 'always',
	proseWrap: 'preserve',
	htmlWhitespaceSensitivity: 'strict',
	endOfLine: 'lf',
	useTabs: true,
	jsxBracketSameLine: true,
}
